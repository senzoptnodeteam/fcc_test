/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

#ifndef _FCC_TEST_H_
#define _FCC_TEST_H_

/*- Includes ---------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*- Definitions ------------------------------------------------------------*/
#define APP_CONFIG_NUM           5

/*- Types ------------------------------------------------------------------*/
typedef struct
{
  uint32_t     magic;

  uint8_t      local;

  uint8_t      channel;
  uint8_t      modulation;
  uint8_t      txPower;
  uint8_t      antenna;
  uint8_t      trim;
  uint8_t      ccNumber;
  uint8_t      ccBand;
  uint8_t      txFlt;

  uint8_t      side;
  uint32_t     duration;

  uint32_t     pulseOn;
  uint32_t     pulseOff;
} AppConfig_t;


/********************* Device Selection ************************************/
#define START_CW	


/*- Variables --------------------------------------------------------------*/
extern AppConfig_t appConfig[APP_CONFIG_NUM];

/*- Prototypes -------------------------------------------------------------*/
void appPhyInit(void);
void appCommandReq(uint8_t *data, uint8_t size);
void appUartSend(char *data, uint8_t size);
void appUartSendStr(char *data);

void appConfigLoad(void);
void appConfigSave(void);
void appConfigSetDefaults(AppConfig_t *cfg);

#endif // _FCC_TEST_H_
