/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "fcc_test.h"
#include "commands.h"
#include "rf.h"

/*- Definitions ------------------------------------------------------------*/
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

/*- Types ------------------------------------------------------------------*/
typedef struct
{
  char *name;
  char *args;
  char *help;
  void (*handler)(void);
} AppCommandDesc_t;

/*- Prototypes -------------------------------------------------------------*/
static void cmdHelpHandler(void);
static void cmdListHandler(void);
static void cmdSaveHandler(void);
static void cmdLoadHandler(void);
static void cmdResetHandler(void);
static void cmdShowHandler(void);
static void cmdLocalHandler(void);
static void cmdRemoteHandler(void);
static void cmdChHandler(void);
#ifdef PHY_AT86RF212
static void cmdModHandler(void);
#endif
static void cmdPwrHandler(void);
static void cmdAntHandler(void);
static void cmdTrimHandler(void);
static void cmdCcNumHandler(void);
static void cmdCcBandHandler(void);
static void cmdTxFltHandler(void);
static void cmdSetHandler(void);
static void cmdPulseHandler(void);
static void cmdPrbsHandler(void);
static void cmdDataHandler(void);
static void cmdStopHandler(void);

/*- Variables --------------------------------------------------------------*/
static AppCommandDesc_t commands[] =
{
  { "help", "[<comamnd>]",
      "\thelp help :)", cmdHelpHandler },

  { " ", "", "", NULL },

  { "list", "",
      "\tList all configurations and their settings.", cmdListHandler },
  { "save", "[<config>]",
      "\tSave current configuration to <config> (default 0).", cmdSaveHandler },
  { "load", "[<config>]",
      "\tLoad configuration from <config> (default 0).", cmdLoadHandler },
  { "reset", "[<config>]",
      "\tReset configuration to defaults.", cmdResetHandler },
  { "show", "[<config>]",
      "\tShow settings for the configuration <config> (default 0).", cmdShowHandler },

  { " ", "", NULL, NULL },

  { "local", "",
      "\tSwitch to a local control mode", cmdLocalHandler },
  { "remote", "",
      "\tSwitch to a remote control mode", cmdRemoteHandler },

  { " ", "", NULL, NULL },

  { "ch", "<channel>",
      "\tSet channel.", cmdChHandler },
#ifdef PHY_AT86RF212
  { "mod", "<modulation>",
      "\tSet modulation expressed as a raw register value (TRX_CTRL_2).\n"
      "\tCommonly used values are:\n"
      "\t  0x00 - BPSK-20\n"
      "\t  0x04 - BPSK-40\n"
      "\t  0x14 - BPSK-40-ALT\n"
      "\t  0x08 - OQPSK-SIN-RC-100\n"
      "\t  0x0c - OQPSK-SIN-250\n"
      "\t  0x1c - OQPSK-RC-250\n",
      cmdModHandler },
#endif
  { "pwr", "<tx_power>",
      "\tSet TX power; <tx_power> expressed as a raw register value.", cmdPwrHandler },
  { "ant", "<antenna>",
      "\tSet antenna diversity configuration:\n"
      "\t 0 - disabled\n"
      "\t 1 - antenna 1\n"
      "\t 2 - antenna 2\n"
      "\t 3 - automatic\n", cmdAntHandler },
  { "trim", "<value>",
      "\tSet Xtal trim value.", cmdTrimHandler },
  { "ccnum", "<value>",
      "\tSet CC_NUMBER value.", cmdCcNumHandler },
  { "ccband", "<value>",
      "\tSet CC_BAND value.", cmdCcBandHandler },
  { "txflt", "<value>",
      "\tSet PLL_TX_FLT value.", cmdTxFltHandler },

  { " ", "", NULL, NULL },

  { "set", "[<duration>] [<side>]",
      "\tSet default values for the parameters of 'cw' and 'prbs' commands.\n"
#ifdef PHY_AT86RF212
      "\t<side> must be 0 for Fch-0.1 MHz, and 1 for Fch+0.1 MHz\n"
#else
      "\t<side> must be 0 for Fch-0.5 MHz, and 1 for Fch+0.5 MHz\n"
#endif
      "\t<duration> value is in ms; set to 0 for indefinite test.", cmdSetHandler },
  { "pulse", "<on_time> <off_time>",
      "\tSet parameters from the pulse operation\n"
      "\t<on_time> <off_time> values are in ms.", cmdPulseHandler },

  { " ", "", NULL, NULL },

  { "cw", "[<side>] [<duration>]",
      "\tStart CW transmission using selected TRX configuration\n"
      "\tsee 'help set' command for paramter defaults.", cmdCwHandler },
  { "prbs", "[<duration>]",
      "\tStart PRBS transmission using selected TRX configuration\n"
      "\tsee 'set' command for paramter defaults.", cmdPrbsHandler },
  { "data", "[<size>] [<interval>]",
      "\tStart normal data transmission test using selected TRX configuration.\n"
      "\tData frames with specified payload size (default 20, max 116) are transmitted\n"
      "\tto an unreachable address with requested acknowledgment using TX_AACK mode.\n"
      "\t<interval> specifies the delay between the transmission attempts (default 0 ms).",
      cmdDataHandler },
  { "stop", "",
      "\tStop the test. Success depends on whether the DUT can receive commands,\n"
      "\tso it will not work during the uninterrupted CW transmission, but it will\n"
      "\twork during the Off state of the pereodic transmission.\n"
      "\tThis command also stops the test running on a local device.", cmdStopHandler },
};

static char *appCommandsPtr = NULL;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
void appCommandResponse(const char *fmt, ...)
{
  char str[512];
  va_list ap;

  va_start(ap, fmt);
  vsnprintf(str, sizeof(str), fmt, ap);
  va_end(ap);

  appUartSendStr(str);
}

/*************************************************************************//**
*****************************************************************************/
static bool isSpace(char c)
{
  return ' ' == c || '\t' == c || 0 == c;
}

/*************************************************************************//**
*****************************************************************************/
char *appCommandNextToken(void)
{
  static char token[32];
  uint8_t ptr;

  while (isSpace(*appCommandsPtr))
  {
    if (0 == *appCommandsPtr)
      return NULL;

    appCommandsPtr++;
  }

  for (ptr = 0; ptr < sizeof(token)-1; ptr++)
  {
    if (isSpace(*appCommandsPtr))
      break;

    token[ptr] = *appCommandsPtr++;
  }

  token[ptr] = 0;

  return token;
}

/*************************************************************************//**
*****************************************************************************/
static bool appCommandUint32(char *tok, uint32_t *val)
{
  char *end;

  *val = strtoul(tok, &end, 0);

  if (0 != *end || ULONG_MAX == *val)
  {
    appCommandResponse("Error: invalid integer: %s", tok);
    return false;
  }

  return true;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdHelpHandler(void)
{
  char *cmd;

  cmd = appCommandNextToken();

  if (cmd)
  {
    for (uint8_t i = 0; i < ARRAY_SIZE(commands); i++)
    {
      if (0 == strcmp(commands[i].name, cmd))
      {
        appCommandResponse("%s \t %s\n", commands[i].name, commands[i].args);
        appCommandResponse(commands[i].help);
        return;
      }
    }
    appCommandResponse("Unknown command '%s'", cmd);
  }
  else
  {
    for (uint8_t i = 0; i < ARRAY_SIZE(commands); i++)
      appCommandResponse("%s \t %s\n", commands[i].name, commands[i].args);
  }
}

/*************************************************************************//**
*****************************************************************************/
static bool getConfigurationNumber(uint8_t *cfg)
{
  char *tok;
  uint32_t val = 0;

  tok = appCommandNextToken();

  if (tok)
  {
    if (!appCommandUint32(tok, &val))
      return false;

    if (val >= APP_CONFIG_NUM)
    {
      appCommandResponse("Error: invalid configuration number");
      return false;
    }
  }

  *cfg = val;

  return true;
}

/*************************************************************************//**
*****************************************************************************/
static void showConfiguration(uint8_t cfg)
{
  appCommandResponse("\t%s, ch = %d (0x%x), TX power = %d (0x%x), antenna = %d\n",
      appConfig[cfg].local ? "local" : "remote", appConfig[cfg].channel,
      appConfig[cfg].channel, appConfig[cfg].txPower, appConfig[cfg].txPower,
      appConfig[cfg].antenna);

#ifdef PHY_AT86RF212
  appCommandResponse("\tmodulation = %d (0x%02x)\n", appConfig[cfg].modulation,
      appConfig[cfg].modulation);
#endif

  appCommandResponse("\tXtal Trim = %d, CC_NUMBER = %d, CC_BAND = %d, PLL_TX_FLT = %d\n",
      appConfig[cfg].trim, appConfig[cfg].ccNumber, appConfig[cfg].ccBand,
      appConfig[cfg].txFlt);

  appCommandResponse("\tduration = %lu ms, side = %d\n",
      appConfig[cfg].duration, appConfig[cfg].side);

  appCommandResponse("\tpulse: On time = %lu ms, Off time = %lu ms",
      appConfig[cfg].pulseOn, appConfig[cfg].pulseOff);
}

/*************************************************************************//**
*****************************************************************************/
static void cmdListHandler(void)
{
  for (uint8_t i = 0; i < APP_CONFIG_NUM; i++)
  {
    appCommandResponse("--- %d ---\n", i);
    showConfiguration(i);
    appCommandResponse("\n");
  }
}

/*************************************************************************//**
*****************************************************************************/
static void cmdSaveHandler(void)
{
  uint8_t cfg;

  if (!getConfigurationNumber(&cfg))
    return;

  appConfig[cfg] = appConfig[0];
  appConfigSave();
}

/*************************************************************************//**
*****************************************************************************/
static void cmdLoadHandler(void)
{
  uint8_t cfg;

  if (!getConfigurationNumber(&cfg))
    return;

  appConfig[0] = appConfig[cfg];
  appConfigSave();
}

/*************************************************************************//**
*****************************************************************************/
static void cmdResetHandler(void)
{
  uint8_t cfg;

  if (!getConfigurationNumber(&cfg))
    return;

  appConfigSetDefaults(&appConfig[cfg]);
  appConfigSave();
}

/*************************************************************************//**
*****************************************************************************/
static void cmdShowHandler(void)
{
  uint8_t cfg;

  if (!getConfigurationNumber(&cfg))
    return;

  showConfiguration(cfg);
}

/*************************************************************************//**
*****************************************************************************/
static void cmdLocalHandler(void)
{
  appConfig[0].local = 1;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdRemoteHandler(void)
{
  appConfig[0].local = 0;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdChHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: channel number required");

  if (!appCommandUint32(tok, &value))
    return;

#ifdef PHY_AT86RF212
  if (value > 9)
    return appCommandResponse("Error: invalid channel number");
#else
  if (value < 10 || value > 26)
    return appCommandResponse("Error: invalid channel number");
#endif

  appConfig[0].channel = value;
}

/*************************************************************************//**
*****************************************************************************/
#ifdef PHY_AT86RF212
static void cmdModHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: modulation value required");

  if (!appCommandUint32(tok, &value))
    return;

  if (value > 0x3f)
    return appCommandResponse("Error: invalid modulation value");

  appConfig[0].modulation = value;
}
#endif

/*************************************************************************//**
*****************************************************************************/
static void cmdPwrHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: TX power level required");
 
  if (!appCommandUint32(tok, &value))
    return;

#ifndef PHY_AT86RF212
  if (value > 0x0f)
    return appCommandResponse("Error: invalid TX power level");
#endif

  appConfig[0].txPower = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdAntHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: antenna number required");
 
  if (!appCommandUint32(tok, &value))
    return;

  if (value > 3)
    return appCommandResponse("Error: invalid antenna number");

  appConfig[0].antenna = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdTrimHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: Xtal trim value required");
 
  if (!appCommandUint32(tok, &value))
    return;

  if (value > 15)
    return appCommandResponse("Error: invalid value");

  appConfig[0].trim = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdCcNumHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: CC_NUMBER value required");
 
  if (!appCommandUint32(tok, &value))
    return;

  if (value > 255)
    return appCommandResponse("Error: invalid value");

  appConfig[0].ccNumber = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdCcBandHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: CC_BAND value required");
 
  if (!appCommandUint32(tok, &value))
    return;

  if (value > 255)
    return appCommandResponse("Error: invalid value");

  appConfig[0].ccBand = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdTxFltHandler(void)
{
  char *tok;
  uint32_t value;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: PLL_TX_FLT value required");
 
  if (!appCommandUint32(tok, &value))
    return;

  if (value > 1)
    return appCommandResponse("Error: invalid value");

  appConfig[0].txFlt = value;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdSetHandler(void)
{
  char *tok;
  uint32_t side;

  if (NULL == (tok = appCommandNextToken()))
  {
    appCommandResponse("duration = %lu ms, side = %d",
        appConfig[0].duration, appConfig[0].side);
    return;
  }

  if (!appCommandUint32(tok, &appConfig[0].duration))
    return;

  if (NULL == (tok = appCommandNextToken()))
    return;

  if (!appCommandUint32(tok, &side))
    return;

  if (side > 1)
    return appCommandResponse("Error: invalid side value");

  appConfig[0].side = side;
}

/*************************************************************************//**
*****************************************************************************/
static void cmdPulseHandler(void)
{
  char *tok;
  uint32_t pulseOn, pulseOff;

  if (NULL == (tok = appCommandNextToken()))
  {
    appCommandResponse("pulse: On time = %lu ms, Off time = %lu ms",
        appConfig[0].pulseOn, appConfig[0].pulseOff);
    return;
  }

  if (!appCommandUint32(tok, &pulseOn))
    return;

  if (NULL == (tok = appCommandNextToken()))
    return appCommandResponse("Error: both parameters are required");

  if (!appCommandUint32(tok, &pulseOff))
    return;

  if ((pulseOn > 0 && 0 == pulseOff) || (0 == pulseOn && pulseOff > 0))
    return appCommandResponse("Error: invalid parameters");

  appConfig[0].pulseOn = pulseOn;
  appConfig[0].pulseOff = pulseOff;
}

/*************************************************************************//**
*****************************************************************************/
void cmdCwHandler(void)
{
  char *tok;
  AppCmdTest_t cmd;
  uint32_t side = appConfig[0].side;
  uint32_t duration = appConfig[0].duration;

  if ((tok = appCommandNextToken()))
  {
    if (!appCommandUint32(tok, &side))
      return;

    if (side > 1)
      return appCommandResponse("Error: invalid side value");
  }

  if ((tok = appCommandNextToken()))
  {
    if (!appCommandUint32(tok, &duration))
      return;
  }

  cmd.cmd        = APP_CMD_CW;
  cmd.channel    = appConfig[0].channel;
  cmd.modulation = appConfig[0].modulation;
  cmd.txPower    = appConfig[0].txPower;
  cmd.antenna    = appConfig[0].antenna;
  cmd.trim       = appConfig[0].trim;
  cmd.ccNumber   = appConfig[0].ccNumber;
  cmd.ccBand     = appConfig[0].ccBand;
  cmd.txFlt      = appConfig[0].txFlt;
  cmd.side       = side;
  cmd.duration   = duration;
  cmd.pulseOn    = appConfig[0].pulseOn;
  cmd.pulseOff   = appConfig[0].pulseOff;

  appCommandReq((uint8_t *)&cmd, sizeof(AppCmdTest_t));
}

/*************************************************************************//**
*****************************************************************************/
static void cmdPrbsHandler(void)
{
  char *tok;
  AppCmdTest_t cmd;
  uint32_t duration = appConfig[0].duration;

  if ((tok = appCommandNextToken()))
  {
    if (!appCommandUint32(tok, &duration))
      return;
  }

  cmd.cmd        = APP_CMD_PRBS;
  cmd.channel    = appConfig[0].channel;
  cmd.modulation = appConfig[0].modulation;
  cmd.txPower    = appConfig[0].txPower;
  cmd.antenna    = appConfig[0].antenna;
  cmd.trim       = appConfig[0].trim;
  cmd.ccNumber   = appConfig[0].ccNumber;
  cmd.ccBand     = appConfig[0].ccBand;
  cmd.txFlt      = appConfig[0].txFlt;
  cmd.side       = 0;
  cmd.duration   = duration;
  cmd.pulseOn    = appConfig[0].pulseOn;
  cmd.pulseOff   = appConfig[0].pulseOff;

  appCommandReq((uint8_t *)&cmd, sizeof(AppCmdTest_t));
}

/*************************************************************************//**
*****************************************************************************/
static void cmdDataHandler(void)
{
  char *tok;
  AppCmdTest_t cmd;
  uint32_t size = 20;
  uint32_t interval = 0;

  if ((tok = appCommandNextToken()))
  {
    if (!appCommandUint32(tok, &size))
      return;
  }

  if ((tok = appCommandNextToken()))
  {
    if (!appCommandUint32(tok, &interval))
      return;
  }

  if (size > 116)
    return appCommandResponse("Error: invalid size value");

  cmd.cmd        = APP_CMD_DATA;
  cmd.channel    = appConfig[0].channel;
  cmd.modulation = appConfig[0].modulation;
  cmd.txPower    = appConfig[0].txPower;
  cmd.antenna    = appConfig[0].antenna;
  cmd.trim       = appConfig[0].trim;
  cmd.ccNumber   = appConfig[0].ccNumber;
  cmd.ccBand     = appConfig[0].ccBand;
  cmd.txFlt      = appConfig[0].txFlt;
  cmd.duration   = appConfig[0].duration;
  cmd.size       = size;
  cmd.interval   = interval;

  appCommandReq((uint8_t *)&cmd, sizeof(AppCmdTest_t));
}

/*************************************************************************//**
*****************************************************************************/
static void cmdStopHandler(void)
{
  AppCmdStop_t cmd;

  appRfTestStop();

  cmd.cmd      = APP_CMD_STOP;

  appCommandReq((uint8_t *)&cmd, sizeof(AppCmdStop_t));
}

/*************************************************************************//**
*****************************************************************************/
uint8_t *_sbrk(int incr)
{
  static uint8_t heap[200];
  static uint8_t *heap_end;
  uint8_t *prev_heap_end;

  if (0 == heap_end)
    heap_end = heap;

  prev_heap_end = heap_end;

  if ((heap_end + incr) >= (heap + sizeof(heap)))
    return NULL;

  heap_end += incr;
  return prev_heap_end;
}

/*************************************************************************//**
*****************************************************************************/
void appCommandsProcessLine(char *data, uint8_t size)
{
  char *cmd;

  appCommandsPtr = data;

  if (NULL == (cmd = appCommandNextToken()))
    return;

  for (uint8_t i = 0; i < ARRAY_SIZE(commands); i++)
  {
    if (0 == strcmp(commands[i].name, cmd))
    {
      commands[i].handler();
      return;
    }
  }

  appCommandResponse("Unknown command. See 'help' for additional information");

  (void)size;
}
