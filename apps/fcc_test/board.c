/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include "phy.h"
#include "halGpio.h"
#include "board.h"

/*- Definitions ------------------------------------------------------------*/
#if defined(HAL_ATSAMD21J18)
  #define PM_APBCMASK_RFCTRL       (1 << 21)
  #define MMIO_REG(mem_addr, type) (*(volatile type *)(mem_addr))
  #define RFCTRL_FECTRL            MMIO_REG(0x42005400, uint16_t)
  #define FECTRL(a1, a2)           ((0/*DIG1*/ << ((a1)*2)) | (1/*DIG2*/ << ((a2)*2)))
#endif

#if defined(PLATFORM_XPLAINED_PRO_SAMR21)
  HAL_GPIO_PIN(ANT1, A, 9);
  HAL_GPIO_PIN(ANT2, A, 12);

  HAL_GPIO_PIN(LED,  A, 19);

#elif defined(PLATFORM_AT08973)
  HAL_GPIO_PIN(LED,  A, 7);

#elif defined(PLATFORM_XPLAINED_PRO_ATMEGA256RFR2)
  HAL_GPIO_PIN(LED,  B, 4);

#elif defined(PLATFORM_XPLAINED_PRO_SAMD20_ZIGBIT)
  HAL_GPIO_PIN(LED,  A, 14);

#elif defined(PLATFORM_XPLAINED_PRO_SAMD21_ZIGBIT)
  HAL_GPIO_PIN(LED,  B, 30);

#else
  #error Unsuported platform
#endif

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
void appBoardSetup(void)
{
  HAL_GPIO_LED_out();
  HAL_GPIO_LED_set();

#if defined(PLATFORM_XPLAINED_PRO_SAMR21)
  HAL_GPIO_ANT1_out();
  HAL_GPIO_ANT1_pmuxen();

  HAL_GPIO_ANT2_out();
  HAL_GPIO_ANT2_pmuxen();

  PORT->Group[HAL_GPIO_PORTA].PMUX[4].bit.PMUXO = PORT_PMUX_PMUXO_F_Val;
  PORT->Group[HAL_GPIO_PORTA].PMUX[6].bit.PMUXE = PORT_PMUX_PMUXE_F_Val;

  PM->APBCMASK.reg |= PM_APBCMASK_RFCTRL;
  RFCTRL_FECTRL = FECTRL(2, 1);
#endif
}

/*************************************************************************//**
*****************************************************************************/
void appBoardLed(bool state)
{
  if (state)
    HAL_GPIO_LED_clr();
  else
    HAL_GPIO_LED_set();
}
