/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <string.h>
#include "sysTypes.h"
#include "eeprom.h"
#include "fcc_test.h"

/*- Definitions ------------------------------------------------------------*/
#if defined(HAL_ATSAMD21J18) || defined(HAL_ATSAMD20J18)
  #define APP_EEPROM_SECTOR_SIZE     256
  #define APP_EEPROM_OFFSET          65536

#elif defined(HAL_ATMEGA256RFR2)
  #define APP_EEPROM_SECTOR_SIZE     64
#else
  #error Unsuported platform
#endif

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
#if defined(HAL_ATSAMD21J18) || defined(HAL_ATSAMD20J18)
void appEepromFlashWrite(uint32_t offset, uint32_t *buf)
{
  NVMCTRL->ADDR.reg = offset >> 1;

  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER;
  while (0 == NVMCTRL->INTFLAG.bit.READY);

  for (uint32_t i = 0; i < APP_EEPROM_SECTOR_SIZE / sizeof(uint32_t); i++)
    *(uint32_t *)(offset + i * sizeof(uint32_t)) = buf[i];
}
#endif

#if defined(HAL_ATMEGA256RFR2)
/*************************************************************************//**
*****************************************************************************/
void appEepromWriteByte(uint16_t address, uint8_t data)
{
  while (EECR & (1 << EEPE));

  EEAR = address;
  EEDR = data;
  ATOMIC_SECTION_ENTER
    EECR |= (1 << EEMPE);
    EECR |= (1 << EEPE);
  ATOMIC_SECTION_LEAVE
}

/*************************************************************************//**
*****************************************************************************/
uint8_t appEepromReadByte(uint16_t address)
{
  while (EECR & (1 << EEPE));

  EEAR = address;
  EECR |= (1 << EERE);
  return EEDR;
}
#endif

/*************************************************************************//**
*****************************************************************************/
#if defined(HAL_ATSAMD21J18) || defined(HAL_ATSAMD20J18)
void appEepromInit(void)
{
  PM->AHBMASK.reg |= PM_AHBMASK_NVMCTRL;
  PM->APBBMASK.reg |= PM_APBBMASK_NVMCTRL;
}

#elif defined(HAL_ATMEGA256RFR2)
void appEepromInit(void)
{
}
#endif

/*************************************************************************//**
*****************************************************************************/
#if defined(HAL_ATSAMD21J18) || defined(HAL_ATSAMD20J18)
void appEepromRead(uint8_t index, uint8_t *data, uint8_t size)
{
  uint32_t offset = APP_EEPROM_OFFSET - ((index+1) * APP_EEPROM_SECTOR_SIZE);
  uint8_t *buf = (uint8_t *)offset;

  memcpy(data, buf, size);
}

#elif defined(HAL_ATMEGA256RFR2)
void appEepromRead(uint8_t index, uint8_t *data, uint8_t size)
{
  uint16_t offset = index * APP_EEPROM_SECTOR_SIZE;

  for (uint16_t i = 0; i < size; i++)
    data[i] = appEepromReadByte(offset + i);
}
#endif

/*************************************************************************//**
*****************************************************************************/
#if defined(HAL_ATSAMD21J18) || defined(HAL_ATSAMD20J18)
void appEepromWrite(uint8_t index, uint8_t *data, uint8_t size)
{
  uint32_t offset = APP_EEPROM_OFFSET - ((index+1) * APP_EEPROM_SECTOR_SIZE);
  uint32_t buf[APP_EEPROM_SECTOR_SIZE / sizeof(uint32_t)];

  memset(buf, 0xff, sizeof(buf));
  memcpy(buf, data, size);

  appEepromFlashWrite(offset, buf);
}

#elif defined(HAL_ATMEGA256RFR2)
void appEepromWrite(uint8_t index, uint8_t *data, uint8_t size)
{
  uint16_t offset = index * APP_EEPROM_SECTOR_SIZE;

  for (uint16_t i = 0; i < size; i++)
  {
    if (appEepromReadByte(offset + i) != data[i])
      appEepromWriteByte(offset + i, data[i]);
  }
}
#endif
