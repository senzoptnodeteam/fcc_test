/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include <string.h>
#include "config.h"
#include "hal.h"
#include "phy.h"
#include "sys.h"
#include "halUart.h"
#include "fcc_test.h"
#include "commands.h"
#include "eeprom.h"
#include "board.h"
#include "rf.h"

/*- Definitions ------------------------------------------------------------*/
#define APP_MAX_PAYLOAD_SIZE     117
#define APP_MAX_CMD_LINE_LENGTH  100
#define APP_MAX_FRAME_RETRIES    10

#define APP_PAN_ID               0xabcd
#define APP_ADDR                 0x0000

/*- Types ------------------------------------------------------------------*/
typedef struct PACK
{
  uint16_t    fcf;
  uint8_t     seq;
  uint16_t    dstPanId;
  uint16_t    dstAddr;
  uint16_t    srcAddr;
  uint32_t    magic;
} AppHeader_t;

typedef struct PACK
{
  AppHeader_t header;
  uint8_t     data[APP_MAX_PAYLOAD_SIZE];
} AppFrame_t;

/*- Variables --------------------------------------------------------------*/
AppConfig_t appConfig[APP_CONFIG_NUM];

static AppFrame_t appFrame;
static uint8_t appFrameSize;
static uint8_t appFrameSeq = 0;
static bool appFrameBusy = false;
static uint8_t appFrameRetries;

static uint8_t appCmdLine[APP_MAX_CMD_LINE_LENGTH + 1];
static uint8_t appCmdLinePtr = 0;

static uint8_t appBuffer[APP_MAX_PAYLOAD_SIZE];
static bool appBufferValid;

volatile uint8_t start_transmission = 0;
volatile uint16_t start_transmission_count = 0;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void appUartStateMachine(uint8_t byte)
{
  if (0x08 == byte)
  {
    if (appCmdLinePtr)
    {
      appUartSendStr("\x08 \x08");
      appCmdLinePtr--;
    }
  }

  else if ('\r' == byte)
  {
    appUartSendStr("\n");

    appCmdLine[appCmdLinePtr] = 0;
    appCommandsProcessLine((char *)appCmdLine, appCmdLinePtr);

    appCmdLinePtr = 0;

    appUartSendStr("\n> ");
  }

  else if ('\n' == byte)
  {
    // skip it
  }

  else if (appCmdLinePtr < APP_MAX_CMD_LINE_LENGTH)
  {
    HAL_UartWriteByte(byte);
    appCmdLine[appCmdLinePtr++] = byte;
  }
}

/*************************************************************************//**
*****************************************************************************/
void HAL_UartBytesReceived(uint16_t bytes)
{
  for (uint16_t i = 0; i < bytes; i++)
  {
    uint8_t byte = HAL_UartReadByte();
    appUartStateMachine(byte);
  }
}

/*************************************************************************//**
*****************************************************************************/
void appUartSend(char *data, uint8_t size)
{
  for (uint8_t i = 0; i < size; i++)
    HAL_UartWriteByte(data[i]);
}

/*************************************************************************//**
*****************************************************************************/
void appUartSendStr(char *data)
{
  while (*data)
    HAL_UartWriteByte(*data++);
}

/*************************************************************************//**
*****************************************************************************/
void PHY_DataConf(uint8_t status)
{
  if (PHY_STATUS_SUCCESS == status)
  {
    appFrameBusy = false;
  }
  else
  {
    if (0 == appFrameRetries--)
    {
      appFrameBusy = false;
      appUartSendStr("Error: remote device is unreachable\r\n");
	  start_transmission = 0;
	  start_transmission_count = 0;
	  
    }
    else
    {
      PHY_DataReq((uint8_t *)&appFrame, appFrameSize);
    }
  }
}

/*************************************************************************//**
*****************************************************************************/
void appDataReq(uint8_t *data, uint8_t size)
{
  if (appFrameBusy)
    return;

  appFrameBusy = true;
  appFrameSize = sizeof(AppHeader_t) + size;
  appFrameRetries = APP_MAX_FRAME_RETRIES;

  appFrame.header.fcf      = 0x8861;
  appFrame.header.seq      = ++appFrameSeq;
  appFrame.header.dstPanId = APP_PAN_ID;
  appFrame.header.dstAddr  = APP_ADDR;
  appFrame.header.srcAddr  = APP_ADDR;
  appFrame.header.magic    = APP_MAGIC;
  memcpy(appFrame.data, data, size);

  PHY_DataReq((uint8_t *)&appFrame, appFrameSize);
}

/*************************************************************************//**
*****************************************************************************/
void PHY_DataInd(PHY_DataInd_t *ind)
{
  AppFrame_t *frame = (AppFrame_t *)ind->data;

  if (ind->size < sizeof(AppHeader_t))
    return;

  if (APP_MAGIC != frame->header.magic)
    return;

  appBufferValid = true;
  memcpy(appBuffer, frame->data, ind->size - sizeof(AppHeader_t));
}

/*************************************************************************//**
*****************************************************************************/
void appPhyInit(void)
{
  PHY_Init();
  PHY_SetPanId(APP_PAN_ID);
  PHY_SetShortAddr(APP_ADDR);
  PHY_SetChannel(APP_DEFAULT_CHANNEL);
#ifdef PHY_AT86RF212
  PHY_SetBand(0);
  PHY_SetModulation(APP_DEFAULT_MODULATION);
#endif
  PHY_SetRxState(true);
}

/*************************************************************************//**
*****************************************************************************/
void appConfigLoad(void)
{
  for (uint8_t i = 0; i < APP_CONFIG_NUM; i++)
    appEepromRead(i, (uint8_t *)&appConfig[i], sizeof(AppConfig_t));
}

/*************************************************************************//**
*****************************************************************************/
void appConfigSave(void)
{
  for (uint8_t i = 0; i < APP_CONFIG_NUM; i++)
    appEepromWrite(i, (uint8_t *)&appConfig[i], sizeof(AppConfig_t));
}

/*************************************************************************//**
*****************************************************************************/
void appConfigSetDefaults(AppConfig_t *cfg)
{
  cfg->magic      = APP_MAGIC;

  cfg->local      = 0;

  cfg->channel    = APP_DEFAULT_CHANNEL;
  cfg->modulation = APP_DEFAULT_MODULATION;
  cfg->txPower    = 0;
  cfg->antenna    = 2;
  cfg->trim       = 0;
  cfg->ccNumber   = 0;
  cfg->ccBand     = 0;
  cfg->txFlt      = 0;

  cfg->side       = 0;
  cfg->duration   = 0;

  cfg->pulseOn    = 0;
  cfg->pulseOff   = 0;
}

/*************************************************************************//**
*****************************************************************************/
static void appCommandReceived(uint8_t *data)
{
  AppCmdTest_t *cmd = (AppCmdTest_t *)data;

  switch (cmd->cmd)
  {
    case APP_CMD_CW:
    case APP_CMD_PRBS:
    {
      appUartSendStr("*** Starting RF test ***\n");
      appRfCwPrbsTestStart(cmd);
    } break;

    case APP_CMD_DATA:
    {
      appUartSendStr("*** Starting data test ***\n");
      appRfDataTestStart(cmd);
    } break;

    case APP_CMD_STOP:
    {
      appUartSendStr("*** Stopping RF test ***\n");
      appRfTestStop();
    } break;

    default:
      break;
  }
}

/*************************************************************************//**
*****************************************************************************/
static void APP_TaskHandler(void)
{
  if (appBufferValid)
  {
    appBufferValid = false;
    appCommandReceived(appBuffer);
  }
}

/*************************************************************************//**
*****************************************************************************/
void appCommandReq(uint8_t *data, uint8_t size)
{
  if (appConfig[0].local)
    appCommandReceived(data);
  else
    appDataReq(data, size);
}

/*************************************************************************//**
*****************************************************************************/
static void appInit(void)
{
  appUartSendStr("\nFCC Remote Test, built " __DATE__ " " __TIME__ " \n");

  appPhyInit();
  appBoardSetup();
  appEepromInit();
  appConfigLoad();

  for (uint8_t i = 0; i < APP_CONFIG_NUM; i++)
  {
    if (APP_MAGIC != appConfig[i].magic)
      appConfigSetDefaults(&appConfig[i]);
  }

  appConfigSave();

  appBufferValid = false;
}

/*************************************************************************//**
*****************************************************************************/
int main(void)
{
  SYS_Init();
  HAL_UartInit(38400);
  appInit();

  while (1)
  {
    SYS_TaskHandler();
    HAL_UartTaskHandler();
    APP_TaskHandler();

#ifdef START_CW	
	if(start_transmission == 0)
	{
		if((start_transmission_count++) >= 1000)
		{
			start_transmission = 1;
			start_transmission_count = 0;
			cmdCwHandler();
			appUartSendStr("Starting\r\n");
		}
	}
#endif
	
  }
}
