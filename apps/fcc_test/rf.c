/**
 *
 * Copyright (C) 2012-2014, Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 * Modification and other use of this code is subject to Atmel's Limited
 * License Agreement (license.txt).
 *
 */

/*- Includes ---------------------------------------------------------------*/
#include "phy.h"
#include "sysTimer.h"
#include "fcc_test.h"
#include "commands.h"
#include "board.h"
#include "rf.h"

/*- Definitions ------------------------------------------------------------*/

/*- Types ------------------------------------------------------------------*/

/*- Prototypes -------------------------------------------------------------*/

/*- Variables --------------------------------------------------------------*/
static SYS_Timer_t appRfDurationTimer;
static SYS_Timer_t appRfPulseTimer;
static SYS_Timer_t appRfIntervalTimer;
static bool appRfState;
static PHY_TestReq_t appRfPhyTestReq;
static uint32_t appRfPulseOn;
static uint32_t appRfPulseOff;
static uint32_t appRfInterval;

/*- Implementations --------------------------------------------------------*/

/*************************************************************************//**
*****************************************************************************/
static void appRfDurationTimerHandler(SYS_Timer_t *timer)
{
  SYS_TimerStop(&appRfPulseTimer);
  SYS_TimerStop(&appRfIntervalTimer);

  appPhyInit();
  appBoardLed(false);

  (void)timer;
}

/*************************************************************************//**
*****************************************************************************/
static void appRfPulseTimerHandler(SYS_Timer_t *timer)
{
  if (appRfState)
  {
    timer->interval = appRfPulseOff;
    appPhyInit();
  }
  else
  {
    timer->interval = appRfPulseOn;
    PHY_TestReq(&appRfPhyTestReq);
  }

  appRfState = !appRfState;
  SYS_TimerStart(timer);
}

/*************************************************************************//**
*****************************************************************************/
void appRfCwPrbsTestStart(AppCmdTest_t *cmd)
{
  if (cmd->duration)
  {
    appRfDurationTimer.interval = cmd->duration;
    appRfDurationTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appRfDurationTimer.handler = appRfDurationTimerHandler;
    SYS_TimerStart(&appRfDurationTimer);
  }

  if (cmd->pulseOn && cmd->pulseOff)
  {
    appRfPulseOn = cmd->pulseOn;
    appRfPulseOff = cmd->pulseOff;

    appRfPulseTimer.interval = appRfPulseOn;
    appRfPulseTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appRfPulseTimer.handler = appRfPulseTimerHandler;
    SYS_TimerStart(&appRfPulseTimer);
  }

  appRfState = true;

  appBoardLed(true);

  appRfPhyTestReq.cw         = APP_CMD_CW == cmd->cmd;
  appRfPhyTestReq.side       = cmd->side;
  appRfPhyTestReq.channel    = cmd->channel;
#ifdef PHY_AT86RF212
  appRfPhyTestReq.modulation = (APP_CMD_CW == cmd->cmd) ? 0x0a : cmd->modulation;
#endif
  appRfPhyTestReq.txPower    = cmd->txPower;
  appRfPhyTestReq.antenna    = cmd->antenna;
  appRfPhyTestReq.trim       = cmd->trim;
  appRfPhyTestReq.ccNumber   = cmd->ccNumber;
  appRfPhyTestReq.ccBand     = cmd->ccBand;
  appRfPhyTestReq.txFlt      = cmd->txFlt;

  PHY_TestReq(&appRfPhyTestReq);
}

/*************************************************************************//**
*****************************************************************************/
static void appRfIntervalTimerHandler(SYS_Timer_t *timer)
{
  PHY_DataTestReq(&appRfPhyTestReq);
  (void)timer;
}

/*************************************************************************//**
*****************************************************************************/
void PHY_DataTestConf(void)
{
  if (appRfInterval)
  {
    appPhyInit();

    appRfIntervalTimer.interval = appRfInterval;
    appRfIntervalTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appRfIntervalTimer.handler = appRfIntervalTimerHandler;
    SYS_TimerStart(&appRfIntervalTimer);
  }
  else
  {
    PHY_DataTestReq(&appRfPhyTestReq);
  }
}

/*************************************************************************//**
*****************************************************************************/
void appRfDataTestStart(AppCmdTest_t *cmd)
{
  if (cmd->duration)
  {
    appRfDurationTimer.interval = cmd->duration;
    appRfDurationTimer.mode = SYS_TIMER_INTERVAL_MODE;
    appRfDurationTimer.handler = appRfDurationTimerHandler;
    SYS_TimerStart(&appRfDurationTimer);
  }

  appRfInterval = cmd->interval;

  appBoardLed(true);

  appRfPhyTestReq.channel    = cmd->channel;
#ifdef PHY_AT86RF212
  appRfPhyTestReq.modulation = cmd->modulation;
#endif
  appRfPhyTestReq.txPower    = cmd->txPower;
  appRfPhyTestReq.antenna    = cmd->antenna;
  appRfPhyTestReq.trim       = cmd->trim;
  appRfPhyTestReq.ccNumber   = cmd->ccNumber;
  appRfPhyTestReq.ccBand     = cmd->ccBand;
  appRfPhyTestReq.txFlt      = cmd->txFlt;
  appRfPhyTestReq.size       = cmd->size;

  PHY_DataTestReq(&appRfPhyTestReq);
}

/*************************************************************************//**
*****************************************************************************/
void appRfTestStop(void)
{
  SYS_TimerStop(&appRfPulseTimer);
  SYS_TimerStop(&appRfDurationTimer);
  SYS_TimerStop(&appRfIntervalTimer);

  appPhyInit();
  appBoardLed(false);
}
